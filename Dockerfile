FROM alerta/alerta-web:6.8.0

LABEL maintainer="te-hung.tseng@esss.se"

RUN /venv/bin/pip install --no-cache-dir python-ldap

