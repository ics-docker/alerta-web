alerta-web
=======

ESS specific Alerta_ Docker_ image.

This image includes python-ldap package

.. _Docker: https://www.docker.com
.. _Alerta: https://hub.docker.com/r/alerta/alerta-web
